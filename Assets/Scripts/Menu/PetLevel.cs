﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PetLevel : MonoBehaviour
{
    private Pets petType;

#pragma warning disable 0649
    [SerializeField] private Image icon;
    [SerializeField] private Button btn;
#pragma warning restore 0649

    private void Awake()
    {
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() => { SceneManager.LoadScene("Game"); });
    }

    public void Initialize(PetData data)
    {
        icon.sprite = data.Icon;
        petType = data.PetType;

        btn.onClick.AddListener(() =>
        {
            PlayerPrefs.SetString("PetLevel", petType.ToString());
        });
    }
}