﻿using UnityEngine;

public class MenuController : MonoBehaviour
{
#pragma warning disable 0649 
    [SerializeField] private Transform levelsContainer;
    [SerializeField] private GameObject petLevel;

    [Space]
    [SerializeField] private PetData[] petsArray;
#pragma warning restore 0649

    private void Start()
    {
        CreateLevels();
    }

    private void CreateLevels()
    {
        for (int i = 0; i < petsArray.Length; i++)
        {
            var pet = Instantiate(petLevel, levelsContainer).GetComponent<PetLevel>();
            pet.Initialize(petsArray[i]);
        }
    }
}