﻿using Spine.Unity;
using UnityEngine;

[CreateAssetMenu(fileName = "PetData", menuName = "Pets/CreatePetData", order = 1)]
public class PetData : ScriptableObject
{
    public Pets PetType;
    public Sprite Icon;
    public Sprite SpriteTail;
    public SkeletonDataAsset SkeletonData;
    public AudioClip AudioQuestion;

    [Space]
    public string AnimationIdle = "Idle";
    public string AnimationJump = "Jump";
    public string AnimationTail = "Tail";
    public string AnimationSad = "Sad";
    public string AnimationNo = "No";

    [Space]
    public string SkinDefault = "default";

    [Space]
    public string SlotTail = "Tail";
}

public enum Pets
{
    Cat, Cow, Dog, Horse, Mouse, Pig
}