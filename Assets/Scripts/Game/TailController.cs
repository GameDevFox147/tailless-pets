﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TailController : MonoBehaviour
{
    private const string pulsationAnim = "Pulsation";
    private const string idleAnim = "Idle";

#pragma warning disable 0649
    [SerializeField] private Image icon;
    [SerializeField] private Button btn;
    [SerializeField] private Animator animator;
#pragma warning restore 0649

    private PetData petData;
    public PetData PetData => petData;

    public void Initialize(PetData data, Action<TailController> callback)
    {
        petData = data;
        icon.sprite = data.SpriteTail;

        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() => { callback?.Invoke(this); });
    }

    public void Backlighting(bool state)
    {
        if (state)
        {
            // подсветка UI?
        }
        else
        {
            // отключение подсветки UI
        }
    }

    public void Pulsation(bool state)
    {
        var animation = state ? pulsationAnim : idleAnim;
        animator.Play(animation);
    }
}