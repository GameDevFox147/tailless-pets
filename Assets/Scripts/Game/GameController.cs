﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private const float offsetY = -340f;

#pragma warning disable 0649
    [SerializeField] private AudioController audioController;

    [Space]
    [SerializeField] private Transform petContainer;

    [Space]
    [SerializeField] private TailController[] tails;

    [Space]
    [SerializeField] private PetData[] petsArray;

    [Space]
    [SerializeField] private Transform finger;

    [Space]
    [SerializeField] private Button homeBtn;
#pragma warning restore 0649

    private PetData petData;
    private SkeletonAnimation skeletonAnim;
    private Spine.Attachment attachmentTail;

    private int failCount;

    private bool gameOver;
    private bool action;

    private void Start()
    {
        gameOver = false;

        homeBtn.onClick.RemoveAllListeners();
        homeBtn.onClick.AddListener(ExitToMenu);

        Button petBtn = petContainer.GetComponent<Button>();
        petBtn.onClick.RemoveAllListeners();
        petBtn.onClick.AddListener(ClickToPet);

        InitializeLevel();

        StartCoroutine(Inaction());
    }

    private void InitializeLevel()
    {
        for (int i = 0; i < tails.Length; i++)
        {
            tails[i].Initialize(petsArray[i], SelectTail);
        }

        var petName = PlayerPrefs.GetString("PetLevel");
        var skeletonData = petsArray[0].SkeletonData;
        petData = petsArray[0];

        Pets petType = (Pets)Enum.Parse(typeof(Pets), petName, true);

        for (int i = 0; i < petsArray.Length; i++)
        {
            if (petsArray[i].PetType == petType)
            {
                petData = petsArray[i];
                skeletonData = petData.SkeletonData;
                audioController.PlayPetQuestion(petData.AudioQuestion);
                break;
            }
        }

        skeletonAnim = SkeletonAnimation.NewSkeletonAnimationGameObject(skeletonData);
        skeletonAnim.loop = true;
        skeletonAnim.skeleton.SetSkin(petData.SkinDefault);

        if (IsHasAnimation(petData.AnimationJump))
        {
            skeletonAnim.AnimationName = petData.AnimationIdle;
        }

        attachmentTail = skeletonAnim.skeleton.FindSlot(petData.SlotTail).Attachment;
        skeletonAnim.skeleton.FindSlot(petData.SlotTail).Attachment = null;

        var tr = skeletonAnim.transform;
        tr.SetParent(petContainer);
        tr.localPosition = new Vector3(tr.localPosition.x, offsetY, tr.localPosition.z);
    }

    private void SelectTail(TailController tailController)
    {
        if (gameOver) return;

        action = true;

        StartCoroutine(IESelectTail(tailController));
    }

    private IEnumerator IESelectTail(TailController tailController)
    {
        if (tailController.PetData == petData)
        {
            gameOver = true;

            audioController.PlayRandomCorrectAction();
            tailController.Backlighting(true);

            skeletonAnim.skeleton.FindSlot(petData.SlotTail).Attachment = attachmentTail;

            var jumpAnimation = skeletonAnim.Skeleton.Data.FindAnimation(petData.AnimationJump);
            if (jumpAnimation != null)
            {
                skeletonAnim.AnimationName = petData.AnimationJump;
                yield return new WaitForSecondsRealtime(jumpAnimation.Duration);
            }

            var tailAnimation = skeletonAnim.Skeleton.Data.FindAnimation(petData.AnimationTail);
            if (tailAnimation != null)
            {
                skeletonAnim.AnimationName = petData.AnimationTail;
                yield return new WaitForSecondsRealtime(tailAnimation.Duration);
            }

            yield return new WaitForSecondsRealtime(1f);

            ExitToMenu();
        }
        else
        {
            audioController.PlayRandomIncorrectAction();

            failCount++;

            if (failCount > 2)
            {
                if (IsHasAnimation(petData.AnimationSad))
                {
                    skeletonAnim.AnimationName = petData.AnimationSad;
                }
            }
            else
            {
                tailController.Backlighting(true);

                skeletonAnim.skeleton.FindSlot(petData.SlotTail).Attachment = attachmentTail;

                var noAnimation = skeletonAnim.Skeleton.Data.FindAnimation(petData.AnimationNo);
                if (noAnimation != null)
                {
                    skeletonAnim.AnimationName = petData.AnimationNo;
                    yield return new WaitForSecondsRealtime(noAnimation.Duration);
                }

                skeletonAnim.skeleton.FindSlot(petData.SlotTail).Attachment = null;

                if (IsHasAnimation(petData.AnimationIdle))
                {
                    skeletonAnim.AnimationName = petData.AnimationIdle;
                }

                tailController.Backlighting(false);
            }
        }
    }

    private IEnumerator Inaction()
    {
        float sec = 0f;

        while (!gameOver)
        {
            if (action)
            {
                action = false;
                sec = 0;

                foreach (var tail in tails)
                {
                    tail.Pulsation(false);
                }
            }

            yield return new WaitForSecondsRealtime(0.1f);

            sec += 0.1f;

            if (sec >= 8f && sec <= 8.1f)
            {
                audioController.PlayPetQuestion(petData.AudioQuestion);

                foreach (var tail in tails)
                {
                    tail.Pulsation(true);
                }
            }
            else if (sec >= 22f && sec <= 22.1f)
            {
                ShowHint();
            }
        }

        foreach (var tail in tails)
        {
            tail.Pulsation(false);
        }

        HideHint();
    }

    private void ShowHint()
    {
        TailController tail = null;

        for (int i = 0; i < tails.Length; i++)
        {
            if (tails[i].PetData == petData)
            {
                tail = tails[i];
                break;
            }
        }

        if (tail == null) return;

        var pos = tail.transform.position;

        finger.position = new Vector3(pos.x, pos.y, pos.z);
        finger.gameObject.SetActive(true);
    }

    private void HideHint()
    {
        finger.gameObject.SetActive(false);
    }

    public void ClickToPet()
    {
        if (gameOver) return;

        action = true;

        StartCoroutine(IEClickToPet());
    }

    public IEnumerator IEClickToPet()
    {
        var tapAnimation = skeletonAnim.Skeleton.Data.FindAnimation("Tap");
        if (tapAnimation != null)
        {
            skeletonAnim.AnimationName = "Tap";
            yield return new WaitForSecondsRealtime(tapAnimation.Duration);
        }

        if (IsHasAnimation(petData.AnimationJump))
        {
            skeletonAnim.AnimationName = petData.AnimationIdle;
        }
    }

    private bool IsHasAnimation(string animation)
    {
        return skeletonAnim.Skeleton.Data.FindAnimation(animation) != null;
    }

    private void ExitToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}