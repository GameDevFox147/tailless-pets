﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private AudioSource audioSource;

    [Space]
    [SerializeField] private AudioClip[] correctActions;
    [SerializeField] private AudioClip[] incorrectActions;
#pragma warning restore 0649

    public void PlayPetQuestion(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void PlayRandomCorrectAction()
    {
        var result = Random.Range(0, correctActions.Length);
        audioSource.clip = correctActions[result];
        audioSource.Play();
    }

    public void PlayRandomIncorrectAction()
    {
        var result = Random.Range(0, incorrectActions.Length);
        audioSource.clip = incorrectActions[result];
        audioSource.Play();
    }
}